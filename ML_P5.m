%% I Uruchomienie poprzedniego skryptu
ML_P4
close all

%% 1. zmienne
v_d = v_max;
h_d = 5200;
hp = 735.49875;
N_d = 94*hp;
n_sd = 2700/60;

rho_hd = rho_0*(1-h_d/44300)^4.256;
T_hd = 15-6.5*h_d/1000;
a_hd = a_0*sqrt((T_hd+273.15)/288);

%% 2. Obliczenie cechy oj�to�ciowej �mig�a
C_D = v_d*(rho_hd/N_d/n_sd^2)^(1/5);
% odczytanie z cha-a
eta_max = .82; % prawa g�rna cha-a
beta_max = deg2rad(20);
J_max = .85;

%% 3. Obliczenie �rednicy �mig�a 
D_s = v_d/J_max/n_sd;

%% 4. Oblicznie Ma
Ma_kl = sqrt(v_d^2+(pi*n_sd*D_s)^2)/a_hd;

%% 5. Tabelka
% 1. Dob�r wektora wysoko�ci
h_max = h_d*1.3;
dH = h_max/5;
H = 0:dH:h_max;

% 2. Odczytanie wektora mocy
H_ft = distdim(H,'m','ft');
N_hp = [180,166,132,118,90,64];
N = N_hp*hp;0

% 3. Obliczenie g�stosci powietrza
Rho =  rho_0*(1-H./44300).^4.256;

% 4. Obliczenie parametru C_n
C_n = N./Rho./n_sd.^3./D_s^5;

% 5. Dob�r wektora pr�dkosci
v_lim = v_d*1.2;
dV = v_lim/7;
V = 0:dV:v_lim;

% 6. Obliczenie J - niezale�ne od H
J = V./n_sd/D_s;

% 7. odczytanie sprawno�1ci
Eta = [[0,.28,.5,.62,.7,.78,.8,.81];
    [0,.3,.5,.64,.7,.79,.8,.84];
    [0,.3,.5,.64,.7,.78,.8,.81];
    [0,.3,.5,.65,.7,.78,.8,.82];
    [0,.3,.52,.65,.75,.8,.81,.82];
    [0,.32,.55,.7,.75,.8,.81,.8]];

% 8.  Macierz mocy
% N_arr(h,N)
N_arr = Eta.*N';

fig_9 = figure(9);
hold on
grid on
p_91 = plot(V,N_arr (1,:),'r');
p_92 = plot(V,N_arr (2,:),'or');
p_93 = plot(V,N_arr (3,:),':r');
p_94 = plot(V,N_arr (4,:),'sr');
p_95 = plot(V,N_arr (5,:),'--r');
p_96 = plot(V,N_arr (6,:),'-.r');
set(p_91,'LineWidth',lSize);
set(p_92,'LineWidth',lSize);
set(p_93,'LineWidth',lSize);
set(p_94,'LineWidth',lSize);
set(p_95,'LineWidth',lSize);
set(p_96,'LineWidth',lSize);
xlabel('v, m/s')
ylabel('N, W')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Charakterystyka pr�dko�ciowo-wysoko�ciowa')
legend('3.1','3.2','3.3','3.4','3.5','3.6');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_9,'fig_9.png')