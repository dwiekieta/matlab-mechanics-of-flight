%% I Uruchomienie poprzedniego skryptu
ML_P2
%close all

%% II Nadpisanie skali - wyznaczenie pola przek. czol. kadluba
rys_jr = 240.81;                                    % [-] wymiar charakterystyczny w jednostkach rys.
rys_skala = rys_m/rys_jr;                   % [m] przelicznik jednostek rys. na m

% II.1 Pole przekroju czolowego kadluba
S_k = (sqrt(1755.6294)*rys_skala)^2;    % [m^2]

% II.2 Dodatkowe dane gabarytowe
l_k = distdim(24,'ft','m');                     % [m] dlugosc calkowita kadluba

% II.3 Przeskalowanie do odczytu dlugosci l_nk
rys_jr = 364.25;                                    % [-] wymiar charakterystyczny w jednostkach rys.
rys_skala = l_k/rys_jr;                         % [m] przelicznik jednostek rys. na m

% II.4 Inne dane
l_nk = 103.52*rys_skala;
v_max = 240*10/36;                              % [m/s] maksymalan predkos samolotu
a_0 = 340.25;                                           % [m/s] pr. dzw. na poz. morza
C_m_sa = -.03;                                      % [-] wsp. mementu podl. plata

% II.5 Przeskalowanie do wyznaczenia pola pow. ster.
rys_m = distdim(11.9,'ft','m');          % [m] rzeczywuisty wymiar charakterystyczny
rys_jr = 89.52;                                    % [-] wymiar charakterystyczny w jednostkach rys.
rys_skala = rys_m/rys_jr;                   % [m] przelicznik jednostek rys. na m

% II.6 Pole pow. ster. poziomych
S_H = (sqrt(2048.8551)*rys_skala)^2;    %[m^2]
b_H = distdim(11.9,'ft','m');                   % [m] rozpiÍtosc usterzenia poziomego
c_0_H = 25.35 * rys_skala;                      % [m] cieciwa przykadlubowa ust. poz.
c_k_H = 16.34 * rys_skala;                      % [m] cieciwa koncowkowa ust. poz.
l_H = 91.02 * rys_skala;                            % [m] odleglosc X_sc do X_sa_H

%% III Obliczenia dla kadluba oplywowego
% III.1 Liczba Reynoldsa kadluba dla v_max
Re_k = v_max*l_k/nu_0;
% III.1.1 Odczytanie parametru c_f dla Re_k
c_f = 0.0025;

% III.2 Oblicznie wydluzenia kadluba
Lambda_k = l_k/sqrt(4*S_k/pi);
% III.3 Odczytanie parametru eta_k dla Lambda_k
eta_k = 1.45;

% III.4 Wydluzenie nosowej czesci kadluba
Lambda_nk = l_nk/sqrt(4*S_k/pi);
% III.5 Obliczenie maksymalnej liczby Macha
Ma = v_max/a_0;
% III.6 Odczytanie parametru eta_nk 
eta_nk = 1;

% III.7 Obliczenie pow. obmywanej
S_ck = 2.85*l_k*sqrt(S_k);

% III.8 Obliczenie oporu kadluba
C_xk = c_f*eta_k*eta_nk*S_ck/S_k;

%% IV Obliczenia usterzenia poziomego
% --------- c_z ---------------------
% IV.1 Odczytanie relacji x/c dla plata 
X_sa_rel = .269;
% IV.2 Wyznaczenie bezwglednej odlegolosci srodka
%       aerodynamicznego plata
X_sa = X_sa_rel * c_a;

% IV.3 Przyjecie wzgl. pol. srodka masy samolotu
X_sc_rel = 0.28;
% IV.4 Bezwzgledne pol. sr. masy samolotu
X_sc = X_sc_rel * c_a;
% IV.4.1 w jedn. rysunkowych
X_sc_jr = X_sc/rys_skala;

% IV.5Obliczenie zbierznosci ust. poz.
lambda_H = c_k_H/c_0_H;
% IV.6 wart. sr. cieciwy aero. ust. poz
c_a_H = 2*c_0_H*(1+lambda_H+lambda_H^2)/3/(1+lambda_H);
% IV.7 Odczytanie relcja x/c dla ust. poz
X_sa_rel_H = .25;
% IV.8 Obliczenie bezwgl. odl. sr. aero. ust. poz.
X_sa_H = X_sa_rel_H * c_a_H;
% IV.8.1 w jedn. rys
X_sa_H_jr = X_sa_H/rys_skala;

% IV.8 Obliczenie cechy obj. ust. poz.
kappa_prim_H = S_H*l_H/S/c_a*.85;
% IV.9 Oblcizenie wsp. sily nosnej dla ust. poz.
c_z_H = C_m_sa/kappa_prim_H+(X_sc_rel-X_sa_rel)*...
    a__c_z_inf(:,2)./kappa_prim_H;

% ---- c_x -------------
% IV.10 Obliczenie wydluzenia ust. pion.
Lambda_H = b_H^2/S_H*.7;
% IV.11 Obliczenia wsp. oporu
c_x_H = .0058 + .005 + c_z_H.^2/pi/Lambda_H;

%% V Obliczenie dla ust. pionowego
% .1 Opor
c_x_v = .0058 + .005;

%%
tabela(:,12) = c_z_H*10;
tabela(:,13) = c_x_H*100;