In this repository are scripts referended to practical classes of modeling stable flight of aircraft
	at WUST faculty of Mechanical and Power Engineering, field of study Mechanical Engineering

	This project is calculate for airplane Mooney M20 with Lycoming O-360-A

	ML_P2 - Aerodynamic characteristics of airfoil
	ML_P3 - Aerodynamic characteristics of airplane
	ML_P4 - Polar of velocity
	ML_P5 - Characteristics of thrust system
	ML_P6 - Performance of airplane
