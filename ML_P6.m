%% I Uruchomienie poprzedniego skryptu
ML_P5
close all

%% 1. Parametry
N_max = 134000; % [W] moc maksymalna silnika

%% 2. Wska�nik obcia�enia mocy
q_N = m_start/N_max*10^3;
pStart = 12;

%% 3. Obliczenie pr�dko�ci samolotu
sqrt_idem = sqrt(2*m_start*g./Rho./S);

sqrt_Cz_prim = sqrt(1./Cz_prim(pStart:pStop));
sqrt_Cz =  sqrt(1./a__c_z_inf(pStart:pStop,2));

% v(Cz, h)
V_prim = sqrt_Cz_prim* sqrt_idem;
V = sqrt_Cz* sqrt_idem;

%% 4. Obliczneie mocy niezb�dnej do lotu poziomego
sqrt_E = sqrt(1./E(pStart:pStop));

% N_n(E,h)
N_n = m_start*g*sqrt_E*sqrt_idem;

%% 5. Odczytanie mocy rozporz�dzalnej
% N_r(h,V)
N_r = [[10.25,9,8.5,8.25,7.75];
    [9.75,8.75,8.25,8,7.6];
    [7.75,7,6.5,6.4,6.25];
    [6.8,6.5,6.1,5.9,5.75];
    [5.45,5.25,5,4.75,4.5];
    [3.9,3.9,3.8,3.7,3.6]]*10^4;

% N_r(V,h)
N_r = N_r';

%% 6. Nadmiar ci�gu
% Delta_N(Cz,h)
Delta_N = N_r-N_n;

%% 7. Obliczenie pr�dkosci wznoszenia
% W(Cz,h)
W = Delta_N./m_start./g;

%% 8. Obliczenie k�ta toru
% Gamma(Cz,h)
Gamma = W./V_prim;

%% 9. Wykresy
fig_10 = figure(10);
hold on
grid on
p_101 = plot(V_prim,W);
set(p_101,'LineWidth',lSize);
xlabel('v, m/s')
ylabel('w, m/s')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Charakterystyka pr�dko�ciowa')
legend('h=0m','h=1352m','h=2704m','h=4056m','h=5408m','h=6760m');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_10,'fig_10.png')


fig_11 = figure(11);
hold on
grid on
p_111 = plot(V_prim,rad2deg(Gamma));
set(p_111,'LineWidth',lSize);
xlabel('v, m/s')
ylabel('\gamma, ^o')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Charakterystyka k�towa')
legend('h=0m','h=1352m','h=2704m','h=4056m','h=5408m','h=6760m');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_11,'fig_11.png')