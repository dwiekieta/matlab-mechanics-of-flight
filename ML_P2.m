clear all
close all
clc

fSize = 30;      % wielkosc liter
lSize = 2;         % gr. linii

pStart = 6;
pStop = 16;

%% I Dane samolotu
% I.1 Proporcje rysunkowe
rys_m = distdim(35.3,'ft','m');          % [m] rzeczywuisty wymiar charakterystyczny
rys_jr = 200.55;                                    % [-] wymiar charakterystyczny w jednostkach rys.
rys_skala = rys_m/rys_jr;                   % [m] przelicznik jednostek rys. na m

% I.2 Dane gabarytowe
b = distdim(35,'ft','m');                 % [m] rozpietosc skrzydel
c_0 = 35.85*rys_skala;                       % [m] ceciwa przykadlubowa
c_k = 19.94*rys_skala;                       % [m] cieciwa koncowkowa
S = distdim(sqrt(167),'ft','m')^2;    % [m^2] pow. nosna

% I.3 Geometria skrzydel
v_x0 = deg2rad(0);                           % [rad] kat skosu krawedzi natarcia 
beta = 3;                                           % [st] 1/4 cieciwy
c_z_max = 1.42;                                 % [-] wsp. sily nosnej

% I.4 Dane masowe
m_start = 1168;                                 % [kg] max masa startowa

% I.5 Inne
g = 9.81;                                              % [m/s^2] przyspieszenie ziemskie
rho_0 = 0.1250*g;                               % [kg/m^3] gestosc pow. na poz. morza
nu_0 = 14.55*10^-6;                           % [m^2/s] lepkosc kin. pow. na poz. morza

%% II Geometria plata
% II.1 Wydluzenie plata
Lambda = b^2/S;
% II.2 Zbierznosc plata
lambda = c_k/c_0;
% II.3 wart. sr. cieciwy aero.
c_a = 2*c_0*(1+lambda+lambda^2)/3/(1+lambda);
% II.4 pol. sr. cieciwy aero. w pl. XY
x_N = b*tan(v_x0)*(1+2*lambda)/6/(1+lambda);

%% III Charakterystyki profilu plata
% III.1 Predkosc przeciagniecia
v_s1 = (2*m_start*g/rho_0/S/c_z_max)^(1/2);
% III.2 Liczba Reynoldsa dla v_s1
Re_1 = v_s1*c_a/nu_0;
% III.3 Odczytanie danych z char. tunelowej
% dla Re = 3.0x10^6
a__c_z_inf = [[-19,-0.95];
    [-18,-1.2];
    [-16,-1.4];
    [-14,-1.35];
    [-12,-1.2];
    [-10,-1];
    [-8,-0.8];
    [-6,-0.55];
    [-4,-0.3];
    [-2,-0.1];
    [0,0.1];
    [2,0.4];
    [4,0.6];
    [6,0.85];
    [8,1];
    [10,1.2];
    [12,1.35];
    [14,1.42];
    [16,1.38];
    [18,1.28]];

fig_1 = figure(1);
p_1 = plot(a__c_z_inf(:,1),a__c_z_inf(:,2));
set(p_1,'LineWidth',lSize);
grid on
hold on
xlabel('K�t natarcia, st.')
ylabel('c_z')
title('Wsp�czynnik si�y no�nej p�ata')
set(gca,'FontSize',20)
set(gca,'LineWidth',2)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_1,'fig_2.png')

% III.4 Oczdytanie wartosci c_x_inf dla c_z_inf
c_x_inf = [.0135,-1,-1,-1,-1,.014,.0118,.0095,...
    .0075,.0059,.0059,.006,.0065,.0099,.0115,...
    .014,-1,-1,-1,0.015];

figure(2)
plot(a__c_z_inf(:,2),c_x_inf)

% III.5 Zanalezienie wartosci c_z_max
c_z_max = max(a__c_z_inf(:,2));
% III.6 Znalezienie wartosci c_x_min
c_x_min_1 = min(abs(c_x_inf));

% III.7 Obliczenie min. wsp. op. aero.
c_x_min_2 = c_x_min_1*(Re_1/10^7)^0.11;
% III.8 Obliczenie poprawki Dc_x_Re
Dc_x_Re = (c_x_min_2-c_x_min_1)*(1-abs(a__c_z_inf(:,2)/c_z_max));
% III.9 Obliczenie skorygowanego wsp. op. aero.
c_x_prim = c_x_inf' + Dc_x_Re;

%% IV Charakterystyka plata
% IV.1 Regresja liniowa c_z
c_z_reg = fitlm(deg2rad(a__c_z_inf(pStart:pStop,1)),a__c_z_inf(pStart:pStop,2));
a = 6.4328;
bc_z = 0.12727;

% IV.2 Obliczenie poprawek
d1 = 0.0537*Lambda/a-0.005;
d2 = -0.43*lambda^5+1.83*lambda^4-3.06*lambda^3+...
    2.56*lambda^2-lambda+0.148;
d3 =(-2.2*10^-7*Lambda^3+10^-7*Lambda^2+1.6*10^-5)*beta^3+1;
d = d1*d2 * d3 / 0.048;

tau1 = .023*(Lambda/a)^3-.103*(Lambda/a)^2+.25*(Lambda/a);
tau2 = -.18*lambda^5+1.52*lambda^4-3.51*lambda^3+...
    3.5*lambda^2-1.33*lambda+0.17;
tau = tau1 * tau2 / 0.17;

% IV.3 Obliczenie indukowanego kata natarcia
alpha_i = rad2deg((a__c_z_inf(:,2)/pi/Lambda)*(1+tau));
% IV.3.1 Oblicznie kata natarcia plata
alpha_p = a__c_z_inf(:,1)+alpha_i;


c_z_reg_p = fitlm(deg2rad(alpha_p(pStart:pStop,1)),a__c_z_inf(pStart:pStop,2));
a_p = 4.9774;
bc_z_p = 0.098437;

fig_3 = figure( 3);
grid on
hold on
%p_3 = plot(c_z_reg);
p_3 = plot(deg2rad(a__c_z_inf(pStart:pStop,1)),c_z_reg.Fitted(:),'--r');
p_3_1 = plot(deg2rad(alpha_p(pStart:pStop,1)),c_z_reg_p.Fitted(:),'r');
%p_3_1 = plot(c_z_reg_p);
set(p_3,'LineWidth',lSize);
set(p_3_1,'LineWidth',lSize);
xlabel('K�t natarcia, rad')
ylabel('c_z')
title('Regresja liniowa wsp�czynnika si�y no�nej')
l_1 = sprintf('Profil, y=%.4fx+%.5f',a,bc_z);
l_2 = sprintf('P�at, y=%.4fx+%.5f',a_p,bc_z_p);
legend(l_1,l_2)
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_3,'fig_1.png')

% IV.4 Obliczenie wsp. op. ind.
c_x_i = (a__c_z_inf(:,2).^2/pi/Lambda)*(1+d);

% IV.5 Przyjecie wsp. Dc_x_tech
c_x_prim_min = min(abs(c_x_prim));
Dc_x_tech = 0.15 * c_x_prim_min;

% IV.6 Obliczenie wsp. oporu dla plata
c_x_p_prim = c_x_prim+Dc_x_tech+c_x_i;

fig_4 = figure(4);
grid on
hold on
p_4_1 = plot(c_x_prim(pStart:pStop),a__c_z_inf(pStart:pStop,2),'--r');
p_4_2 = plot(c_x_p_prim(pStart:pStop),a__c_z_inf(pStart:pStop,2),'r');
set(p_4_1,'LineWidth',lSize);
set(p_4_2,'LineWidth',lSize);
xlabel('c_x')
ylabel('c_z')
title('Zale�no�� wsp�czynnik�w aerodynamicznych')
legend('Profil','P�at')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_4,'fig_3.png')

fig_5=figure(5);
subplot(1,2,1)
grid on
hold on
p_5 = plot(a__c_z_inf(:,1),a__c_z_inf(:,2),'--r');
p_5_1 = plot(alpha_p,a__c_z_inf(:,2),'r');
set(p_5,'LineWidth',lSize);
set(p_5_1,'LineWidth',lSize);
xlabel('K�t natarcia, st.')
ylabel('c_z')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Wsp�czynnik si�y no�nej p�ata')
legend('Profil','P�at')

subplot(1,2,2)
grid on
hold on
p_5_2 = plot(c_x_prim(pStart:pStop),a__c_z_inf(pStart:pStop,2),'--r');
p_5_3 = plot(c_x_p_prim(pStart:pStop),a__c_z_inf(pStart:pStop,2),'r');
set(p_5_2,'LineWidth',lSize);
set(p_5_3,'LineWidth',lSize);
xlabel('c_x')
ylabel('c_z')
title('Zale�no�� wsp�czynnik�w aerodynamicznych')
legend('Profil','P�at')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_5,'fig_4.png')

%% V Tabela wynikowa
tabela = [[1:20]', a__c_z_inf(:,2), a__c_z_inf(:,1),...
    c_x_inf', Dc_x_Re, c_x_prim,a__c_z_inf(:,2),...
    alpha_i,alpha_p,c_x_i,...
    c_x_p_prim];

