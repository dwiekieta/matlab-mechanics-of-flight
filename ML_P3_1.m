%% I Uruchomienie poprzedniego skryptu
ML_P3
%close all

%% II skala
% pow. boczna ust. pion.
rys_m = distdim(24,'ft','m');       % [m] dl. kadluba
rys_jr = 364.25;                              % [-] dl. kadluba
rys_skala = rys_m/rys_jr; 

S_v = 3.5*(sqrt(1251.7959)*rys_skala)^2;    % [m^2] pow. ust. pion.

%% III tabela 3.1
% 1. Tabele wymiar�w podstawowych
% [Cxj, Sj]
C_S = [...
    C_xk, S_k;          % kad�ub
    c_x_v, S_v;           % ust. pionowe
    0.01,1;                 % rura wydechowa (2)
    0.08,0.0014;        % zastrza�y tylni 
    0.68,0.002];        % cylindry z boku silnika

% 2. Iloczyn CS
% [(Cx*S)j]
C_S(:,3) = C_S(:,1).*C_S(:,2);
% 2.1 Suma iloczyn�w
CS_sum = sum(C_S(:,3));

%% II Opory szkodliwe samolotu
% 1. Minimalny op�r szkodliwy 3.7
Cx_szk_min = CS_sum/S;

% 2. K�towy op�r szkodliwy 3.8
% 2.1 Wsp�czynnik opor�w szkodliwych
xi = 4.5;
% 2.2 
Cx_szk = Cx_szk_min*(1+abs(a__c_z_inf(:,2))./xi);

%% III Opory kompletnego samolotu
% 1. Wsp�czynnik interferencyjny
Ki = 0.09;
% 2. Op�r kompletny 3.9
Cx = (c_x_p_prim + Cx_szk +...
    S_H/S*c_x_H) * (1+Ki);

%% IV Wsp�czynnik si�y no�nej
% 1. 3.10
Cz_prim = a__c_z_inf(:,2)+ S_H/S*c_z_H;

% 2. Aproksymacja
% 2.1 Punkty wykresu [Cz,Cx]
X_fZ = [Cz_prim(pStart:pStop),...
    Cx(pStart:pStop)];
% 2.2 Macierz Z
Z_11 = sum(X_fZ(:,1).^4);
Z_12 = sum(X_fZ(:,1).^2);
Z_21 = sum(X_fZ(:,1).^2);
Z_22 = length(X_fZ(:,1));
Z = [[Z_11 Z_12];[Z_21, Z_22]];
% 2.3 Macierz C
C_1 = sum(X_fZ(:,2).*X_fZ(:,1).^2);
C_2 = sum(X_fZ(:,2));
C = [C_1;C_2];
% 2.4 Obliczenie wsp. X
X = Z^-1*C;
% 2.5 Obliczenie Lambda_e
Lambda_e = 1/pi/X(1);
% 2.6 obliczenie e
e = Lambda_e/Lambda;

%% V Wsp�czynniki KE
% 1. Doskona�o�� aerodynamiczna D
D = Cz_prim./Cx;
% 2. Aerodynamiczna funkcja energ. E
E =Cz_prim.^3./Cx.^2;

%%
tabela(:,14) = Cx_szk*100;
tabela(:,15) = Cx*100;
tabela(:,16) = Cz_prim *10;
tabela(:,17) = D;
tabela(:,18) = E;

%%
fig_5;
subplot(1,2,1)
p_5_4 = plot(alpha_p,Cz_prim,':r');
set(p_5_4,'LineWidth',lSize);
legend('Profil','P�at','Samolot')

subplot(1,2,2)
p_5_5 = plot(Cx(pStart:pStop),Cz_prim(pStart:pStop),':r');
set(p_5_5,'LineWidth',lSize);
legend('Profil','P�at','Samolot')
saveas(fig_5,'fig_4.png')

fig_6 = figure(6);
subplot(1,2,1)
grid on
hold on
p_6_1 = plot(alpha_p(pStart:pStop),D(pStart:pStop),'r');
set(p_6_1,'LineWidth',lSize);
xlabel('\alpha')
ylabel('D')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
title('Funkcja doskona�o�ci')

subplot(1,2,2)
grid on
hold on
p_6_2 = plot(alpha_p(pStart:pStop),E(pStart:pStop),'r');
set(p_6_2,'LineWidth',lSize);
xlabel('\alpha')
ylabel('E')
title('Funkcja energetyczna')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_6,'fig_5.png')

fig_7 = figure(7);
grid on
hold on
p_7_1 =plot(X_fZ(:,1),X_fZ(:,2),'r');
t = -1.5:0.02:1.5;
x = X(1)*t.^2+X(2);
p_7_2 = plot(t,x,':r');
set(p_7_1,'LineWidth',lSize);
set(p_7_2,'LineWidth',lSize);
xlabel('C_z')
ylabel('C_x')
title('Funkcja aproksymacyjna')
l_3 = sprintf('y=%.5fx^2+%.3f',X(1),X(2));
legend('c_x[c_z]',l_3);
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_7,'fig_6.png')