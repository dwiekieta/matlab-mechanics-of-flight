%% I Uruchomienie poprzedniego skryptu
ML_P3_1
close all

%% II. Wsp�czynniki sta�e
% 1. Tabela zbiorcza 1
C_ZX = linearCz(c_z_max,0.05,15)';
C_ZX(:,2) = X(1)*C_ZX(:,1).^2+X(2);
C_ZX(:,3) = sqrt(1./sqrt(C_ZX(:,1).^2+C_ZX(:,2).^2));
C_ZX(:,4) = sqrt(C_ZX(:,2).^2./sqrt(C_ZX(:,1).^2+C_ZX(:,2).^2).^3);
C_ZX(:,5) = atan(C_ZX(:,2)./C_ZX(:,1));

% 2. Wyznaczenie mas po�rednich
m_netto = 629;      % [kg]
m_paliwa = 181;    % [kg]
m_czlowieka = (m_start-m_netto-m_paliwa)/4;
m_min = m_netto+2*m_czlowieka;
m_sr = m_min+1/2*m_paliwa;

% 3. Oblcizneie mno�nik�w
M = sqrt(2*[m_min,m_sr,m_start].*g/rho_0/S);

% 4. Tabela zbiorcza 2
C_VW = C_ZX(:,3).*M(1);
C_VW(:,2) = C_ZX(:,4).*M(1);
C_VW(:,3) = C_ZX(:,3).*M(2);
C_VW(:,4) = C_ZX(:,4).*M(2);
C_VW(:,5) = C_ZX(:,3).*M(3);
C_VW(:,6) = C_ZX(:,4).*M(3);

f_1_app = approx(C_VW(:,1),C_VW(:,2),[1 1 1]);
f_1_a = tangent00(f_1_app);
f_1_gamma = atan(f_1_a);

f_2_app = approx(C_VW(:,3),C_VW(:,4),[1 1 1]);
f_2_a = tangent00(f_2_app);
f_2_gamma = atan(f_2_a);

f_3_app = approx(C_VW(:,5),C_VW(:,6),[1 1 1]);
f_3_a = tangent00(f_3_app);
f_3_gamma = atan(f_3_a);

tt = 0:100;
fig_8 = figure(8);
hold on
grid on
p_8_2 = plot(tt,tt*f_1_a,'r');
p_8_1 = plot(C_VW(:,1),C_VW(:,2),':r');
p_8_3 = plot(C_VW(:,3),C_VW(:,4),'--r');
p_8_4 = plot(C_VW(:,5),C_VW(:,6),'-.r');
set(p_8_1,'LineWidth',lSize);
set(p_8_2,'LineWidth',lSize);
set(p_8_3,'LineWidth',lSize);
set(p_8_4,'LineWidth',lSize);
xlabel('v, m/s')
ylabel('w, m/s')
set(gca,'FontSize',fSize)
set(gca,'LineWidth',lSize)
set(gca, 'YDir','reverse')
title('Biegunowa pr�dko�ci')
l_4 = sprintf('y=%.3fx',f_1_a);
legend(l_4,'w_1','w_2','w_3');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
saveas(fig_8,'fig_7.png')


%% Funkcje programu
function z_out = linearCz(Cz_begin,Cz_end,n)
    Cz_range = Cz_begin - Cz_end;
    Cz_diff = Cz_range/n;
    z_out = Cz_begin:-Cz_diff:Cz_end+Cz_diff;
end

function fun = approx(X,Y,polynomial)
    n = length(polynomial);      % wileko��  fn aprox
    p = 2*(n-1);           % maksymalna pot�ga
    
    x = [];
    for i =1:n
        for j = 1:n
            x(i,j) = sum(X.^(p-i-j+2));
        end
    end
    
    y = [];
    for i = 1:n
        y(i) = sum(Y.*X.^(n-i));
    end
    
    fun = x^-1*y';
end

function a = tangent00(fun)
    n = length(fun);
    x = (fun(n)/fun(1))^(1/(n-1));
    
    A = [];
    for i = 1:n
        A(i) = fun(i)*x^(n-1-i);
    end
    
    a = sum(A);
end